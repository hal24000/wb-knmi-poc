import base64
from io import BytesIO

import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from data.database import db

def plot_locations():
    query = {}
    project = {"_id":0, "station_name":1, "station_latitude":1, "station_longitude":1, "station_id":1, 'station_height':1}
    df = pd.DataFrame(db['knmi_weather_station_data'].find(query, project))
    stations = df[~df.duplicated(subset=['station_name'], keep='first')]
    fig = px.scatter_mapbox(
        data_frame=stations,
        lat="station_latitude",
        lon="station_longitude",
        hover_name="station_name",
        hover_data=['station_id', 'station_height'],
        zoom=1
    )
    fig.update_layout(mapbox_style="open-street-map", margin={"r":0,"t":0,"l":0,"b":0})
    
    return fig


def make_stations_list():
    query = {}
    project = {"_id":0, "station_name":1}
    df = pd.DataFrame(db['knmi_weather_station_data'].find(query, project))
    stations = df[~df.duplicated(subset=['station_name'], keep='first')]
    stations =sorted(stations['station_name'].to_list())
    return stations


def make_schema_dict():
    schema_dict = {
        'v_dd': 'dd - Wind Direction 10 Min Average',
        'v_ff': 'ff - Wind Speed at 10m 10 Min Average',
     'v_gff': 'gff - Wind Gust at 10m 10 Min Maximum',
     'v_ta': 'ta - Air Temperature 1 Min Average',
     'v_rh': 'rh - Relative Humidity 1 Min Average',
     'v_pp': 'pp - Air Pressure at Sea Level 1 Min Average',
     'v_zm': 'zm - Meteorological Optical Range 10 Min Average',
     'v_d1h': 'D1H - Rainfall Duration in last Hour',
     'v_dr': 'dr - Precipitation Duration (Rain Gauge) 10 Min Sum',
     'v_hc': 'hc - Cloud Base',
     'v_hc1': 'hc1 - Cloud Base First Layer',
     'v_hc2': 'hc2 - Cloud Base Second Layer',
     'v_hc3': 'hc3 - Cloud Base Third Layer',
     'v_nc': 'nc - Total cloud cover',
     'v_nc1': 'nc1 - Cloud Amount First Layer',
     'v_nc2': 'nc2 - Cloud Amount Second Layer',
     'v_nc3': 'nc3 - Cloud Amount Third Layer',
     'v_pg': 'pg - Precipitation Intensity (PWS) 10 Min Average',
     'v_pr': 'pr - Precipitation Duration (PWS) 10 Min Sum',
     'v_qg': 'qg - Global Solar Radiation 10 Min Average',
     'v_r12h': 'R12H - Rainfall in last 12 Hours',
     'v_r1h': 'R1H - Rainfall in last Hour',
     'v_r24h': 'R24H - Rainfall in last 24 Hours',
     'v_r6h': 'R6H - Rainfall in last 6 Hours',
     'v_rg': 'rg - Precipitation Intensity (Rain Gauge) 10 Min Average',
     'v_ss': 'ss - Sunshine Duration',
     'v_td': 'td - Dew Point Temperature 1.5m 1 Min Average',
     'v_tgn': 'tgn - Grass Temperature 10cm 10 Min Minimum',
     'v_tgn12': 'Tgn12 - Grass Temperature Minimum last 12 Hours',
     'v_tgn14': 'Tgn14 - Grass Temperature Minimum last 14 Hours',
     'v_tgn6': 'Tgn6 - Grass Temperature Minimum last 6 Hours',
     'v_tn': 'tn - Ambient Temperature 1.5m 10 Min Minimum',
     'v_tn12': 'Tn12 - Air Temperature Minimum last 12 Hours',
     'v_tn14': 'Tn14 - Air Temperature Minimum last 14 Hours',
     'v_tn6': 'Tn6 - Air Temperature Minimum last 6 Hours',
     'v_tx': 'tx - Ambient Temperature 1.5m 10 Min Maximum',
     'v_tx12': 'Tx12 - Air Temperature Maximum last 12 Hours',
     'v_tx24': 'Tx24 - Air Temperature Maximum last 24 Hours',
     'v_tx6': 'Tx6 - Air Temperature Maximum last 6 Hours',
     'v_ww': 'ww - wawa Weather Code',
     'v_pwc': 'pwc - Present Weather',
     'v_ww_10': 'ww-10 - wawa Weather Code for Previous 10 Min Interval',
     'v_ts1': 'ts1 - Number of Lightning Discharges at Station',
     'v_ts2': 'ts2 - Number of Lightning Discharges near Station'
    }
    
    inv_schema_dict = {v:k for k, v in schema_dict.items()}
    return schema_dict, inv_schema_dict


def plot_feature(station_name, feature_name, schema_dict):
    query = {
    "station_name":station_name}
    project = {
        "_id":0,
        "measurement_timestamp":1,
        feature_name:1,
    }
    df = pd.DataFrame(db['knmi_weather_station_data'].find(query, project))
    df = df.set_index("measurement_timestamp").sort_index()

    fig = make_subplots(subplot_titles=[schema_dict[feature_name]])
    fig.add_trace(go.Scatter(x=df.index, y=df[feature_name]))
    
    fig["layout"].update(
        margin=dict(l=20, r=20, b=0, t=20),
        height=260,
    )
    return fig
    

def make_dataframe(station_name):
    query = {
    "station_name":station_name}
    project = {
        "_id":0,
    }
    df = pd.DataFrame(db['knmi_weather_station_data'].find(query, project))
    return df


def download_df(df):
    """
    Generates a link allowing the data in a given Pandas dataframe to be downloaded
    
    Args:
        df (pandas.core.frame.DataFrame): Dataframe of assets
        
    Returns:
        href string

    """
    val = to_excel(df)
    b64 = base64.b64encode(val)  # val looks like b'...'
    return f'<a href="data:application/octet-stream;base64,{b64.decode()}" download="knmi_data.xlsx">Download data</a>'  # decode b'abc' => abc


def to_excel(df):
    """
    Takes dataframe and converts to bytes which to be downloaded
    
    Args:
        df (pandas.core.frame.DataFrame): Dataframe of assets
    
    Returns:
        processed_data (bytes)
        
    """
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine="xlsxwriter")
    df.to_excel(writer, sheet_name="Sheet1")
    writer.save()
    processed_data = output.getvalue()
    return processed_data