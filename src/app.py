import streamlit as st
from data.database import db
from visualization.plot import download_df, make_dataframe, make_schema_dict, make_stations_list, plot_feature, plot_locations

#### Set page structure ####
st.set_page_config(layout="wide", initial_sidebar_state="collapsed")
hide_streamlit_style = """
            <style>
            footer {visibility: hidden;}
            </style>
            """
st.markdown(hide_streamlit_style, unsafe_allow_html=True)
st.markdown(
    f"""
<style>
    .reportview-container .main .block-container{{
        padding-top: 0;
    }}
</style>
""",
    unsafe_allow_html=True,
)
hide_decoration_bar_style = '''
    <style>
        header {visibility: hidden;}
    </style>
'''
st.markdown(hide_decoration_bar_style, unsafe_allow_html=True)
##################

st.header("W+B KNMI Demo")
c0_1, c0_2 = st.beta_columns(2)
with c0_1:
    with st.beta_expander("Map of Stations", expanded=True):
        st.plotly_chart(plot_locations(), use_container_width=True)
with c0_2:
    with st.beta_expander("Weather Data", expanded=True):
        schema_dict, inv_schema_dict = make_schema_dict()
        select_site = st.selectbox("Station", make_stations_list(), index=1)
        select_feature = st.selectbox("Feature", sorted(list(inv_schema_dict.keys())))
        st.plotly_chart(plot_feature(select_site, inv_schema_dict[select_feature], schema_dict), use_container_width=True)
with st.beta_expander("Raw Data", expanded=True):
    data_df = make_dataframe(select_site)
    st.markdown(
        f"{download_df(data_df)}.",
        unsafe_allow_html=True,
    )
    st.dataframe(data_df)
